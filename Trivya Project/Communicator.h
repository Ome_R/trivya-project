#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include <map>
#include <Windows.h>

class Communicator {
public:
	void bindAndListen();
	void handleRequests();
private:
	std::map<SOCKET, IRequestHandler> m_clients;
	RequestHandlerFactory m_requestHandler;

	void startThreadForNewClients();

};