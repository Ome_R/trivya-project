#pragma once
#include "IRequestHandler.h"
#include <vector>

class JsonRequestPacketDeserializer {
public:
	static LoginRequest deserializeLoginRequest(std::vector<Byte> buffer);
	static SignupRequest deserializeSignupRequest(std::vector<Byte> buffer);
};