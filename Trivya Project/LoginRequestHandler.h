#pragma once
#include "IRequestHandler.h"
#include "LoginManager.h"
#include "RequestHandlerFactory.h"

class LoginRequestHandler : public IRequestHandler {
public:
	bool isRequestRelevant();
	RequestResult handleRequest(Request request);
private:
	LoginManager m_loginManager;
	RequestHandlerFactory m_handlerFactory;

	RequestResult login(Request request);
	RequestResult signup(Request request);
};