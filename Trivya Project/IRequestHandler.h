#pragma once
#include <string>
#include <ctime>
#include <vector>

typedef unsigned char Byte;

class IRequestHandler {
	virtual bool isRequestRelevant(Request request) = 0;
	virtual RequestResult handleRequest(Request request) = 0;
};

typedef struct RequestResult {
	char* response;
	IRequestHandler* newHandler;
} RequestResult;

typedef struct Request {
	int id;
	std::vector<Byte> buffer;
} Request;

typedef struct LoginRequest {
	std::string username;
	std::string password;
} LoginRequest;

typedef struct SignupRequest {
	std::string username;
	std::string password;
	std::string email;
} SignupRequest;

typedef struct LoginResponse {
	unsigned int status;
} LoginResponse;

typedef struct SignupResponse {
	unsigned int status;
} SignupResponse;

typedef struct ErrorResponse {
	std::string message;
} SignupResponse;