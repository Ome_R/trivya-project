#pragma once
#include "IDatabase.h"
#include "RequestHandlerFactory.h"
#include "Communicator.h"

class Server {
public:
	void run();
private:
	IDatabase m_database;
	RequestHandlerFactory m_requestHandler;
	Communicator m_communicator;
};