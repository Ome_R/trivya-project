#pragma once
#include "IRequestHandler.h"
#include <vector>

class JsonResponsePacketSerializer {
public:
	static std::vector<Byte> serializeResponse(LoginResponse loginResponse);
	static std::vector<Byte> serializeResponse(SignupResponse signupResponse);
	static std::vector<Byte> serializeResponse(ErrorResponse errorResponse);
};