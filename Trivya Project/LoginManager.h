#pragma once
#include "LoggedUser.h"
#include "IDatabase.h"
#include <string>
#include <vector>

class LoginManager {
public:
	void signup(std::string email, std::string username, std::string password);
	void login(std::string username, std::string password);
	void logout();
private:
	IDatabase m_database;
	std::vector<LoggedUser> m_loggedUsers;
};