#pragma once
#include "LoginManager.h"

class RequestHandlerFactory {
public:
	int createLoginRequestHandler();
private:
	LoginManager m_loginManager;
};