#pragma once
#include "LoggedUser.h"
#include "Question.h"
#include <map>
#include <list>

class IDatabase {
public:
	std::map<LoggedUser, int> getHighscores();
	bool doesUserExist(std::string username);
	std::list<Question> getQuestions(int amount);
};